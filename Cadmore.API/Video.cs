﻿using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cadmore.API
{
    public static class Video
    {
        public static async Task<List<VideoModel>> Search(string search)
        {
            if (Account.IsLoggedIn)
            {
                return await Data.Repositories.VideoRepository.Search(search, Account.OrganizationId);
            }

            return null;
        }

        public static async Task<VideoModel> Get(Guid id)
        {
            if (Account.IsLoggedIn)
            {
                var video = await Data.Repositories.VideoRepository.Get(id);

                if (video != null && video.OrganizationId == Account.OrganizationId)
                {
                    return video;
                }
            }

            return null;
        }

        public static async Task<VideoModel> Edit(string title, string description, Guid videoId)
        {
            var video = await Get(videoId);

            if (video != null && !await TitleExists(title, videoId))
            {
                return await Cadmore.Data.Repositories.VideoRepository.Edit(title, description, videoId);
            }

            return null;
        }

        public static async Task<bool> TitleExists(string title, Guid? excludeId)
        {
            if (Account.IsLoggedIn)
            {
                return await Data.Repositories.VideoRepository.TitleExists(title, excludeId);
            }

            return true; //defaults to not add if they do not have rights.
        }

        public static async Task<VideoModel> Add(string title, string description)
        {
            if (Account.IsLoggedIn)
            {
                return await Data.Repositories.VideoRepository.Add(title, description, Account.OrganizationId);
            }

            return null;
        }
    }
}
