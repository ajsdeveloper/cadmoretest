﻿using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadmore.API
{
    public static class Customers
    {

        public static async Task<List<CustomerModel>> Search(string name)
        {
            if (Account.IsLoggedIn)
            {
                return await Data.Repositories.CustomerRepository.Search(name, Account.OrganizationId);
            }
            return null;
        }

        public static async Task<bool> DeleteOrg(Guid customerId, Guid organizationId)
        {
            var customer = await Get(customerId);

            if (customer != null)
            {

                var customerOrg = customer.Organizations.FirstOrDefault(x => x.OrganizationId == organizationId);

                if (customerOrg != null)
                {
                    return await Cadmore.Data.Repositories.CustomerRepository.DeleteOrg(customerOrg.Id);
                }

            }
            return false;
        }

        public static async Task<CustomerOrganizationModel> AddOrg(Guid customerId, Guid organizationId)
        {
            var customer = await Get(customerId);

            if (customer != null)
            {
                return await Cadmore.Data.Repositories.CustomerRepository.AddOrg(customerId, organizationId);
            }

            return null;
        }

        public static async Task<CustomerModel> Get(Guid id)
        {

            if (Account.IsLoggedIn)
            {
                var customer = await Cadmore.Data.Repositories.CustomerRepository.Get(id);

                if (customer.Organizations.FirstOrDefault(x => x.OrganizationId == Account.OrganizationId) != null)
                {
                    return customer;
                }

            }

            return null;


        }

    }
}
