﻿using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cadmore.API
{
    public static class Account
    {

        public static async Task<bool> ChangeOrganization(Guid organizationId)
        {
            if (User.Organizations.FirstOrDefault(x => x.OrganizationId == organizationId) != null)
            {
                HttpContext.Current.Session["OrganiationId"] = organizationId;
                return true;
            }
            return false;
        }


        public static bool IsLoggedIn { 
            get {
                return HttpContext.Current.Session["UserId"] != null;    
            }
        }

        public static Guid OrganizationId { 
            get {
                if (IsLoggedIn)
                {
                    return (Guid)HttpContext.Current.Session["OrganizationId"];
                }
                return Guid.Empty;
            }
        }

        public static UserModel User { 
            get
            {
                if (IsLoggedIn)
                {
                    return (UserModel)HttpContext.Current.Session["UserModel"];
                }
                return null;
            }
        }

        public static async Task<bool> Logout()
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
            return true;
        }

        public static async Task<bool> Login(string userName, string password)
        {

            var login = await Cadmore.Data.Repositories.UserRepository.Login(userName, password);

            if (login != null)
            {

                HttpContext.Current.Session["UserModel"] = login;
                HttpContext.Current.Session["UserId"] = login.Id;

                if (login.Organizations.Any())
                {
                    HttpContext.Current.Session["OrganizationId"] = login.Organizations.FirstOrDefault().OrganizationId;
                }
                else
                {
                    HttpContext.Current.Session["OrganizationId"] = null;
                }


                return true;
            }
            return false;

        }

    }
}
