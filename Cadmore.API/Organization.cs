﻿using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.API
{
    public static class Organization
    {

        public static async Task<List<OrganizationModel>> GetAll()
        {
            if (Account.IsLoggedIn)
            {
                var orgs = await Cadmore.Data.Repositories.OrganizationRepository.GetAll();

                var model = new List<OrganizationModel>();

                foreach (var org in Account.User.Organizations)
                {

                    model.Add(orgs.FirstOrDefault(x => x.Id == org.OrganizationId));
                    
                }

                return model;

            }
            return null;
        }

        public static async Task<OrganizationModel> Get(Guid id)
        {
            if (Account.IsLoggedIn && Account.OrganizationId == id)
            {
                return await Cadmore.Data.Repositories.OrganizationRepository.Get(id);
            }

            return null;
        }

    }
}
