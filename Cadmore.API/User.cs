﻿using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.API
{
    public static class User
    {

        public static async Task<bool> DeleteOrg(Guid userId, Guid organizationId)
        {
            var user = await Get(userId);

            if (user != null)
            {

                var userOrg = user.Organizations.FirstOrDefault(x => x.OrganizationId == organizationId);

                if (userOrg != null)
                {
                    return await Cadmore.Data.Repositories.UserRepository.DeleteOrg(userOrg.Id);
                }

            }
            return false;
        }

        public static async Task<UserOrganizationModel> AddOrg(Guid userId, Guid organizationId)
        {
            var user = await Get(userId);

            if (user != null)
            {
                return await Cadmore.Data.Repositories.UserRepository.AddOrg(userId, organizationId);
            }

            return null;
        }

        public static async Task<UserModel> Get(Guid id)
        {

            if (Account.IsLoggedIn)
            {
                var user = await Cadmore.Data.Repositories.UserRepository.Get(id);

                if (user.Organizations.FirstOrDefault(x => x.OrganizationId == Account.OrganizationId) != null)
                {
                    return user;
                }

            }

            return null;


        }

    }
}
