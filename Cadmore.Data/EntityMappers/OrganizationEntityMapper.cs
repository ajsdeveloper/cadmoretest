﻿using Cadmore.Data.Tables;
using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.EntityMappers
{
    public static class OrganizationEntityMapper
    {

        public static OrganizationModel ToModel(Organization entity)
        {
            var model = new OrganizationModel();

            model.Id = entity.Id;
            model.OrganizationName = entity.OrganizationName;

            return model;
        }

    }
}
