﻿using Cadmore.Data.Tables;
using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.EntityMappers
{
    public static class UserEntityMapper
    {

        public static UserModel ToModel(User entity)
        {
            var model = new UserModel();

            model.FirstName = entity.FirstName;
            model.Id = entity.Id;
            model.LastName = entity.LastName;
            model.Password = entity.Password;
            model.UserName = entity.UserName;

            model.Organizations = new List<UserOrganizationModel>();

            if (entity.Organizations != null)
            {
                foreach (var org in entity.Organizations)
                {
                    model.Organizations.Add(UserOrganizationEntityMapper.ToModel(org));
                }
            }

            return model;
        }

    }
}
