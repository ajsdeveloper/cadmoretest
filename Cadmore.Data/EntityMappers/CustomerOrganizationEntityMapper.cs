﻿using Cadmore.Data.Tables;
using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.EntityMappers
{
    public static class CustomerOrganizationEntityMapper
    {

        public static CustomerOrganizationModel ToModel(CustomerOrganization entity)
        {
            var model = new CustomerOrganizationModel();

            model.CustomerId = entity.CustomerId;
            model.Id = entity.Id;
            model.OrganizationId = entity.OrganizationId;

            if (entity.Organization != null)
            {
                model.Organization = OrganizationEntityMapper.ToModel(entity.Organization);
            }

            return model;
        }

    }
}
