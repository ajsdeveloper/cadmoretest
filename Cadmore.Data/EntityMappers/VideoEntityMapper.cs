﻿using Cadmore.Data.Tables;
using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.EntityMappers
{
    public static class VideoEntityMapper
    {

        public static VideoModel ToModel(Video entity)
        {
            var model = new VideoModel();

            model.Description = entity.Description;
            model.Id = entity.Id;
            model.OrganizationId = entity.OrganizationId;
            model.Title = entity.Title;

            if (entity.Organization != null)
            {
                model.Organization = OrganizationEntityMapper.ToModel(entity.Organization);
            }

            return model;
        }

    }
}
