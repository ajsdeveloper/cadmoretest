﻿using Cadmore.Data.Tables;
using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.EntityMappers
{
    public static class UserOrganizationEntityMapper
    {

        public static UserOrganizationModel ToModel(UserOrganization entity)
        {
            var model = new UserOrganizationModel();

            model.Id = entity.Id;
            model.OrganizationId = entity.OrganizationId;
            model.UserId = entity.UserId;

            if (entity.Organization != null)
            {
                model.Organization = OrganizationEntityMapper.ToModel(entity.Organization);
            }

            return model;
        }

    }
}
