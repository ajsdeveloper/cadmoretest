﻿using Cadmore.Data.Tables;
using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.EntityMappers
{
    public static class CustomerEntityMapper
    {

        public static CustomerModel ToModel(Customer entity)
        {
            var model = new CustomerModel();

            model.Email = entity.Email;
            model.FirstName = entity.FirstName;
            model.Id = entity.Id;
            model.LastName = entity.LastName;

            model.Organizations = new List<CustomerOrganizationModel>();

            if (entity.Organizations != null)
            {
                foreach (var org in entity.Organizations)
                {
                    model.Organizations.Add(CustomerOrganizationEntityMapper.ToModel(org));
                }
            }

            return model;
        }

    }
}
