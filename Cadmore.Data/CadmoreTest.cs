﻿using Cadmore.Data.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data
{
    public class CadmoreTest : DbContext
    {

        public CadmoreTest(string connectionString = "Name=DefaultContext") : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
            System.Data.Entity.Database.SetInitializer<CadmoreTest>(null);
        }

        public DbSet<Organization> Organizations { get; set; }

        public DbSet<Video> Videos { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserOrganization> UserOrganizations { get; set; }

        public DbSet<CustomerOrganization> CustomerOrganizations { get; set; }

    }
}
