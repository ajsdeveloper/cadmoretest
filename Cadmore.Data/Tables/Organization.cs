﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.Tables
{
    [Table("[dbo].[Organizations]")]
    public class Organization
    {
        [Key]
        public Guid Id { get; set; }

        public string OrganizationName { get; set; }



    }
}
