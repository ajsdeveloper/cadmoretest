﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.Tables
{
    [Table("[dbo].[Users]")]
    public class User
    {
        [Key]
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; } 

        [ForeignKey("UserId")]
        public virtual List<UserOrganization> Organizations { get; set; }
    }
}
