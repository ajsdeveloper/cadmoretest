﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Data.Tables
{
    [Table("[dbo].[Customers]")]
    public class Customer
    {

        [Key]
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        [ForeignKey("CustomerId")]
        public virtual List<CustomerOrganization> Organizations { get; set; }

    }
}
