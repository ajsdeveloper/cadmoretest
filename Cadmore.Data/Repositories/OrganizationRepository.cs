﻿using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Cadmore.Data.EntityMappers;

namespace Cadmore.Data.Repositories
{
    public static class OrganizationRepository
    {

        public static async Task<List<OrganizationModel>> GetAll()
        {
            using (var db = new CadmoreTest())
            {
                var records = await db.Organizations.OrderBy(o => o.OrganizationName).ToListAsync();

                var model = new List<OrganizationModel>();

                foreach (var record in records)
                {
                    model.Add(OrganizationEntityMapper.ToModel(record));
                }

                return model;
            }

        }

        public static async Task<OrganizationModel> Get(Guid id)
        {
            using (var db = new CadmoreTest())
            {
                var record = await db.Organizations.FirstOrDefaultAsync(x => x.Id == id);

                if (record != null)
                {
                    return OrganizationEntityMapper.ToModel(record);
                }
            }
            return null;
        }


    }
}
