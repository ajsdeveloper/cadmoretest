﻿using Cadmore.Data.EntityMappers;
using Cadmore.Data.Tables;
using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Cadmore.Data.Repositories
{
    public static class VideoRepository
    {

        public static async Task<List<VideoModel>> Search(string search, Guid organizationId)
        {
            using (var db = new CadmoreTest())
            {
                var records = await db.Videos.Where(x => x.OrganizationId == organizationId && (x.Title.ToLower().Contains(search) || x.Description.ToLower().Contains(search))).ToListAsync();

                var model = new List<VideoModel>();

                foreach (var record in records)
                {
                    model.Add(VideoEntityMapper.ToModel(record));
                }

                return model;


            }
        }

        public static async Task<VideoModel> Get(Guid id)
        {
            using (var db = new CadmoreTest())
            {
                var record = await db.Videos
                    .Include(x => x.Organization)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (record != null)
                {
                    return VideoEntityMapper.ToModel(record);
                }
            }
            return null;
        }

        public static async Task<VideoModel> Edit(string title, string description, Guid id)
        {
            using (var db = new CadmoreTest())
            {
                var record = await db.Videos.FirstOrDefaultAsync(x => x.Id == id);

                if (record != null)
                {
                    record.Title = title;
                    record.Description = description;
                    await db.SaveChangesAsync();
                    return VideoEntityMapper.ToModel(record);
                }
            }
            return null;
        }


        public static async Task<bool> TitleExists(string title, Guid? excludeId)
        {
            using (var db = new CadmoreTest())
            {
                if (excludeId != null)
                {
                    return await db.Videos.FirstOrDefaultAsync(x => x.Title.ToLower() == title.ToLower() && x.Id != excludeId) != null;
                }
                else
                {
                    return await db.Videos.FirstOrDefaultAsync(x => x.Title.ToLower() == title.ToLower()) != null;
                }
            }
        }


        public static async Task<VideoModel> Add(string title, string description, Guid organizationId)
        {
            using (var db = new CadmoreTest())
            {
                var dupecheck = await db.Videos.FirstOrDefaultAsync(x => x.Title.ToLower() == title.ToLower() && x.OrganizationId == organizationId);

                if (dupecheck != null)
                {
                    return VideoEntityMapper.ToModel(dupecheck);
                }
                else
                {
                    var record = new Video()
                    {
                        Id = Guid.NewGuid(),
                        Description = description,
                        OrganizationId = organizationId,
                        Title = title
                    };

                    db.Videos.Add(record);
                    await db.SaveChangesAsync();
                    return VideoEntityMapper.ToModel(record);
                }

            }
        }

    }
}
