﻿using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Cadmore.Data.EntityMappers;
using Cadmore.Data.Tables;

namespace Cadmore.Data.Repositories
{
    public static class CustomerRepository
    {

        public static async Task<List<CustomerModel>> Search(string name, Guid organizationId)
        {
            using (var db = new CadmoreTest())
            {

                List<CustomerOrganization> records;
                if (string.IsNullOrEmpty(name))
                {
                    records = await db.CustomerOrganizations
                    .Include(x => x.Customer)
                    .Where(x => x.OrganizationId == organizationId)
                    .ToListAsync();

                }
                else
                {
                    records = await db.CustomerOrganizations
                    .Include(x => x.Customer)
                    .Where(x => x.OrganizationId == organizationId && x.Customer.FirstName.ToLower().Contains(name.ToLower()) || x.Customer.LastName.ToLower().Contains(name.ToLower()))
                    .ToListAsync();
                }


                var model = new List<CustomerModel>();

                foreach (var record in records)
                {
                    model.Add(CustomerEntityMapper.ToModel(record.Customer));
                }

                return model;
            }
        }

        public static async Task<bool> DeleteOrg(Guid id)
        {
            using (var db = new CadmoreTest())
            {
                var record = await db.CustomerOrganizations.FirstOrDefaultAsync(x => x.Id == id);

                if (record != null)
                {
                    db.CustomerOrganizations.Remove(record);
                    await db.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        public static async Task<CustomerOrganizationModel> AddOrg(Guid customerId, Guid organizationId)
        {
            using (var db = new CadmoreTest())
            {
                var dupecheck = await db.CustomerOrganizations.FirstOrDefaultAsync(x => x.CustomerId == customerId && x.OrganizationId == organizationId);

                if (dupecheck != null)
                {
                    return CustomerOrganizationEntityMapper.ToModel(dupecheck);
                }
                else
                {
                    var newRecord = new CustomerOrganization()
                    {
                        Id = Guid.NewGuid(),
                        OrganizationId = organizationId,
                        CustomerId = customerId
                    };

                    db.CustomerOrganizations.Add(newRecord);
                    await db.SaveChangesAsync();
                    return CustomerOrganizationEntityMapper.ToModel(newRecord);
                }
            }
        }


        public static async Task<CustomerModel> Get(Guid id)
        {
            using (var db = new CadmoreTest())
            {
                var record = await db.Customers
                    .Include(x => x.Organizations)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (record != null)
                {
                    return CustomerEntityMapper.ToModel(record);
                }
            }
            return null;
        }

    }
}
