﻿using Cadmore.Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Cadmore.Data.EntityMappers;
using Cadmore.Data.Tables;

namespace Cadmore.Data.Repositories
{
    public static class UserRepository
    {

        public static async Task<UserModel> Login(string userName, string password)
        {
            using (var db = new CadmoreTest())
            {
                var record = await db.Users
                                 .Include(x => x.Organizations)
                                 .FirstOrDefaultAsync(x => x.UserName == userName && x.Password == password);

                if (record != null)
                {
                    return await Get(record.Id);
                }
            }

            return null;
        }

        public static async Task<bool> DeleteOrg(Guid id)
        {
            using (var db = new CadmoreTest())
            {
                var record = await db.UserOrganizations.FirstOrDefaultAsync(x => x.Id == id);

                if (record != null)
                {
                    db.UserOrganizations.Remove(record);
                    await db.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        public static async Task<UserOrganizationModel> AddOrg(Guid userId, Guid organizationId)
        {
            using (var db = new CadmoreTest())
            {
                var dupecheck = await db.UserOrganizations.FirstOrDefaultAsync(x => x.UserId == userId && x.OrganizationId == organizationId);

                if (dupecheck != null)
                {
                    return UserOrganizationEntityMapper.ToModel(dupecheck);    
                }
                else
                {
                    var newRecord = new UserOrganization()
                    {
                        Id = Guid.NewGuid(),
                        OrganizationId = organizationId,
                        UserId = userId
                    };

                    db.UserOrganizations.Add(newRecord);
                    await db.SaveChangesAsync();
                    return UserOrganizationEntityMapper.ToModel(newRecord);
                }
            }
        }

        public static async Task<UserModel> Get(Guid id)
        {
            using (var db = new CadmoreTest())
            {
                var record = await db.Users
                    .Include(x => x.Organizations)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (record != null)
                {
                    return UserEntityMapper.ToModel(record);
                }
            }
            return null;
        }

    }
}
