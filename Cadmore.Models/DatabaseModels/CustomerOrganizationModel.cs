﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Models.DatabaseModels
{
    public class CustomerOrganizationModel
    {

        public Guid Id { get; set; }

        public Guid CustomerId { get; set; }
        public  CustomerModel Customer { get; set; }

        public Guid OrganizationId { get; set; }
        public  OrganizationModel Organization { get; set; }



    }
}
