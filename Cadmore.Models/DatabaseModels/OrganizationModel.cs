﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Models.DatabaseModels
{
    public class OrganizationModel
    {

        public Guid Id { get; set; }

        public string OrganizationName { get; set; }


    }
}
