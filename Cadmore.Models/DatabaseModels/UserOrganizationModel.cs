﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadmore.Models.DatabaseModels
{
    public class UserOrganizationModel
    {

        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public UserModel User { get; set; }

        public Guid OrganizationId { get; set; }
         public OrganizationModel Organization { get; set; }


    }
}
