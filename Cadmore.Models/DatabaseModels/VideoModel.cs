﻿using System;

namespace Cadmore.Models.DatabaseModels
{
    using System.Web.Mvc;

    public class VideoModel
    {
        public Guid Id { get; set; }

        [Remote(action: "VideoTitleAllowed", controller: "Home", AdditionalFields = nameof(Id))]
        public string Title { get; set; }

        public string Description { get; set; }

        public Guid OrganizationId { get; set; }
        
        public OrganizationModel Organization { get; set; }

    }
}
