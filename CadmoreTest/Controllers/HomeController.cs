﻿using Cadmore.API;
using CadmoreTest.Utils;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CadmoreTest.Controllers
{
    using System.Web.UI;

    using Microsoft.Ajax.Utilities;

    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class HomeController : Controller
    {
        public async Task<ActionResult> Login(string redirectUrl = "", string errorMessage = "")
        {
            ViewBag.ErrorMessage = errorMessage;
            ViewBag.RedirectUrl = redirectUrl;
            return View("~/Views/Home/Login.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(string userName, string password, string redirectUrl)
        {
            if (await Cadmore.API.Account.Login(userName, password))
            {
                if (!string.IsNullOrEmpty(redirectUrl))
                {
                    return Redirect(redirectUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { errorMessage = "Login Failed.  Please try again." });
            }
        }

        [UserAuthenticated]
        public async Task<ActionResult> Index()
        {

            ViewBag.Organizations = await Cadmore.API.Organization.GetAll();
            return View("~/Views/Home/Index.cshtml");
        }


        [UserAuthenticated]
        [HttpPost]
        public async Task<ActionResult> ChangeOrganization(Guid organizationId, string redirectUrl)
        {

            await Account.ChangeOrganization(organizationId);

            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        [UserAuthenticated]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddVideo(string title, string description)
        {
            await Cadmore.API.Video.Add(title, description);
            return RedirectToAction("Index", "Home");
        }

        [UserAuthenticated]
        public async Task<ActionResult> _Edit(Guid id)
        {
            var video = await Cadmore.API.Video.Get(id);
            return PartialView("~/Views/Home/_Edit.cshtml", video);
        }

        [UserAuthenticated]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditVideo(string title, string description, Guid videoId)
        {
            await Video.Edit(title, description, videoId);
            return RedirectToAction("Index", "Home");
        }

        [UserAuthenticated]
        public async Task<JsonResult> VideoTitleAllowed(string title, Guid? id)
        {
            return Json(!await Video.TitleExists(title, id), JsonRequestBehavior.AllowGet);
        }
        
        public async Task<JsonResult> IsLoggedIn()
        {
            return Json(Account.IsLoggedIn);
        }

        [UserAuthenticated]
        public async Task<ActionResult> SearchCustomers(string search)
        {
            var results = await Customers.Search(search);
            return PartialView("~/Views/Home/_Customers.cshtml", results);
        }

        [UserAuthenticated]
        public async Task<ActionResult> SearchVideos(string search)
        {
            var results = await Cadmore.API.Video.Search(search);
            return PartialView("~/Views/Home/_Videos.cshtml", results);
        }


        public async Task<ActionResult> LogOut()
        {
            await Account.Logout();
            return RedirectToAction("Login", "Home");
        }

    }
}