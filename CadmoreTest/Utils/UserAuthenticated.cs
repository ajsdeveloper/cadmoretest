﻿using Cadmore.API;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CadmoreTest.Utils
{
    public class UserAuthenticated : AuthorizeAttribute
    {
        private readonly string[] allowedroles;
        public UserAuthenticated(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            return Account.IsLoggedIn;

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.HttpContext.Session["RedirectUrl"] = filterContext.HttpContext.Request.Url.ToString();
            filterContext.Result = new RedirectResult(ConfigurationManager.AppSettings["DefaultLogin"] + "?redirect=" + filterContext.HttpContext.Request.Url.ToString());
            //filterContext.Result = new HttpUnauthorizedResult();
        }

    }
}