USE [CadmoreTest]
GO
/****** Object:  Table [dbo].[CustomerOrganizations]    Script Date: 7/16/2020 5:28:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerOrganizations](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[CustomerId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_CustomerOrganizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 7/16/2020 5:28:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [uniqueidentifier] NOT NULL,
	[LastName] [nvarchar](100) NULL,
	[FirstName] [nvarchar](100) NULL,
	[Email] [nvarchar](500) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organizations]    Script Date: 7/16/2020 5:28:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organizations](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationName] [nvarchar](100) NULL,
 CONSTRAINT [PK_Organizatons] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserOrganizations]    Script Date: 7/16/2020 5:28:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserOrganizations](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NULL,
	[OrganizationId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_UserOrganizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/16/2020 5:28:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Videos]    Script Date: 7/16/2020 5:28:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Videos](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [nvarchar](max) NULL,
	[OrganizationId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Videos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[CustomerOrganizations] ([Id], [OrganizationId], [CustomerId]) VALUES (N'52b52a77-efb3-4662-9cb4-116c1d5f79d5', N'e2ed600c-638e-4c3f-80b0-89c162c7171d', N'3cc2aede-eca7-46a3-a4e1-20cd66803040')
GO
INSERT [dbo].[CustomerOrganizations] ([Id], [OrganizationId], [CustomerId]) VALUES (N'af1ea238-1fc4-4ae2-a472-8c7f75c3637f', N'55e085b1-0101-43b8-a843-e085a7164628', N'e269130e-16a8-4434-a0d4-858c61d0e69b')
GO
INSERT [dbo].[Customers] ([Id], [LastName], [FirstName], [Email]) VALUES (N'3cc2aede-eca7-46a3-a4e1-20cd66803040', N'Romano', N'Alessandro', N'aromano@test.com')
GO
INSERT [dbo].[Customers] ([Id], [LastName], [FirstName], [Email]) VALUES (N'e269130e-16a8-4434-a0d4-858c61d0e69b', N'Kobliska', N'Danek', N'dkobliska@test.com')
GO
INSERT [dbo].[Organizations] ([Id], [OrganizationName]) VALUES (N'e2ed600c-638e-4c3f-80b0-89c162c7171d', N'Acme Inc')
GO
INSERT [dbo].[Organizations] ([Id], [OrganizationName]) VALUES (N'55e085b1-0101-43b8-a843-e085a7164628', N'Videos 4 Sale')
GO
INSERT [dbo].[UserOrganizations] ([Id], [UserId], [OrganizationId]) VALUES (N'79735f7e-256c-49c4-91b0-61036d67cb5e', N'bbdf0334-2017-475a-9e18-0334ac390502', N'e2ed600c-638e-4c3f-80b0-89c162c7171d')
GO
INSERT [dbo].[UserOrganizations] ([Id], [UserId], [OrganizationId]) VALUES (N'98c02ccb-5a66-4cd4-a7b2-7febb5c87f2b', N'27e98451-2d97-42d5-8e58-45772d5fdf7b', N'55e085b1-0101-43b8-a843-e085a7164628')
GO
INSERT [dbo].[UserOrganizations] ([Id], [UserId], [OrganizationId]) VALUES (N'56c3dcf4-f987-490e-8f16-8a7a3e5f500b', N'0959e438-6825-4373-858e-17416c38c71c', N'e2ed600c-638e-4c3f-80b0-89c162c7171d')
GO
INSERT [dbo].[UserOrganizations] ([Id], [UserId], [OrganizationId]) VALUES (N'0baf4154-9b39-49ba-bb7d-c23867849432', N'ab40a0ad-c169-45b9-a5ee-3ae55224d852', N'55e085b1-0101-43b8-a843-e085a7164628')
GO
INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [UserName], [Password]) VALUES (N'bbdf0334-2017-475a-9e18-0334ac390502', N'Aadhya', N'Patel', N'apatel', N'password')
GO
INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [UserName], [Password]) VALUES (N'0959e438-6825-4373-858e-17416c38c71c', N'Margaret', N'Cadmore', N'mcadmore', N'password')
GO
INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [UserName], [Password]) VALUES (N'ab40a0ad-c169-45b9-a5ee-3ae55224d852', N'Akim', N'Dula', N'adula', N'password')
GO
INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [UserName], [Password]) VALUES (N'27e98451-2d97-42d5-8e58-45772d5fdf7b', N'James', N'Hargrave', N'jhargrave', N'password')
GO
INSERT [dbo].[Videos] ([Id], [Title], [Description], [OrganizationId]) VALUES (N'd1bec849-e0b9-45b2-9ca0-20d88a17e7f1', N'Bhaijaan Bajrangi', N'Woodbolly classic.', N'e2ed600c-638e-4c3f-80b0-89c162c7171d')
GO
INSERT [dbo].[Videos] ([Id], [Title], [Description], [OrganizationId]) VALUES (N'cdc58da6-e357-4774-b9d7-2cdb99e2463b', N'Samurai Last', N'A movie starring watanabe ken and cruise tom.', N'e2ed600c-638e-4c3f-80b0-89c162c7171d')
GO
INSERT [dbo].[Videos] ([Id], [Title], [Description], [OrganizationId]) VALUES (N'bafb4927-86fb-4eec-9419-52452786a166', N'Best Movie Ever 3', N'Most epic.  Most legendary.  Most classic.', N'55e085b1-0101-43b8-a843-e085a7164628')
GO
INSERT [dbo].[Videos] ([Id], [Title], [Description], [OrganizationId]) VALUES (N'6d86fb82-64d2-4f9c-8591-83536744aaed', N'Best Movie Ever', N'An epic, legendary, classic.', N'55e085b1-0101-43b8-a843-e085a7164628')
GO
INSERT [dbo].[Videos] ([Id], [Title], [Description], [OrganizationId]) VALUES (N'c9b4c682-7d96-4126-bd75-b699d2a6bdc3', N'Wind with the Gone', N'Nobody can tell what this is about.', N'e2ed600c-638e-4c3f-80b0-89c162c7171d')
GO
INSERT [dbo].[Videos] ([Id], [Title], [Description], [OrganizationId]) VALUES (N'0761f05f-e474-47ec-9692-f9ffb3074afd', N'Little Chicken', N'A movie about anything other than a chicken.', N'e2ed600c-638e-4c3f-80b0-89c162c7171d')
GO
INSERT [dbo].[Videos] ([Id], [Title], [Description], [OrganizationId]) VALUES (N'e9285203-13f2-4a69-b2db-fb42283f9676', N'Best Movie Ever 2', N'More epic.  More legendary.  More classic.', N'55e085b1-0101-43b8-a843-e085a7164628')
GO
